#/usr/bin/bash

ALL_PATCHES=$( ls unsorted |sort -n |egrep -o '^[0-9]+') 

SQL="SELECT ver_id from versions order by id desc limit 1"

USER="root"

HOST="127.0.0.1"

PASS="MY_PASS"

DB_NAME="mydb"

#DB_VER=$( echo $SQL|mysql -u $USER -h $HOST -p$PASS $DB_NAME )
# need some sort of filter to get version of db from that output
# for example: awk "BEGIN {FS=\"|\";}{print \$2;}"
DB_VER="5"

LAST_DB_VER=''
for x in $ALL_PATCHES
do
	if [ $x -gt $DB_VER ]
	then
		#cat unsorted/$(ls unsorted|egrep "^$x.+") |mysql -u $USER -h $HOST -p$PASS $DB_NAME 
		echo $(ls unsorted|egrep "^$x.+")
		let LAST_DB_VER=$x
	fi	
done

if [ $LAST_DB_VER != '' ]
then
	echo $LAST_DB_VER

	SQL_UPDATE="INSERT INTO versions(ver_id) VALUES ($LAST_DB_VER)"

	#echo $SQL_UPDATE|mysql -u $USER -h $HOST -p$PASS $DB_NAME 
fi
