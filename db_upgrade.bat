@echo OFF
REM #install bash 
REM #run bash script
REM #(PS: test machines running windows are not available at this moment)
set host="127.0.0.1"
set user="username"
set passwd="pass"
set db_name="mydb"
set sql="SELECT ver_id from versions order by id desc limit 1"
REM set db_ver= `echo %sql% ^| path\to\mysql -u %user% -h %host% -p%passwd% %db_name% `
REM # no idea how to parse ver from output
set db_ver=5
set last_ver=o
for /R unsorted %%i in (*) do (
	if /I "%db_ver%" LSS %%i (
		REM type unsorted\%%i~ | path\to\mysql -u %user% -h %host% -p%passwd% %db_name%
		echo %%i
		set last_ver=%%i
	)
)
if not "%last_ver%"=="o" (
	REM echo "INSERT INTO versions(ver_id) VALUES (%last_ver%)" |  path\to\mysql -u %user% -h %host% -p%passwd% %db_name%
	echo %last_ver%
)
